<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/shops', 'Api\ShopsController@index');
Route::get('/shops/{id}', 'Api\ShopsController@show');

Route::put('/shops/{shop_id}/books/{book_id}', 'Api\ShopsController@update');

Route::get('/publishers/{publisher}/shops', 'Api\ShopsController@index');
