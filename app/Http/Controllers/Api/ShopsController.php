<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\Publisher;
use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Publisher $publisher)
    {
        if ($publisher) {
            return response()->json(['shops' => Shop::whereHas('books_in_stock', function ($query) use ($publisher) {
                return $query->where('publisher_id', $publisher->id);
            }, '>=', 1)->with(['books_in_stock' => function ($query) use ($publisher) {
                $query->where('publisher_id', $publisher->id);
            }])->orderBy('books_sold_count', 'DESC')->get()]);
        }

        return response()->json(['shops' => Shop::orderBy('books_sold_count', 'DESC')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Shop::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Shop::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $shop_id, $book_id)
    {
        $shop = Shop::findOrFail($shop_id);
        if ($request->has('sold') && $request->get('sold') > 0) {
            $book = Book::findOrFail($book_id);
            if ($book->copies_in_stock >= $request->get('sold')) {
                $book->copies_in_stock -= $request->get('sold');
                $book->save();
                $shop->books_sold_count += $request->get('sold');
            }
        }
        $shop->update($request->all());
        return $shop;
//        return request()->json(['shop' => $shop]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = Shop::findOrFail($id);
        $shop->delete();
    }
}
