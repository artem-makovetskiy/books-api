<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'shops';


    protected $fillable = [
        'name'
    ];

    public function books_in_stock()
    {
        return $this->belongsToMany('App\Book', 'book_shop');
    }
}
