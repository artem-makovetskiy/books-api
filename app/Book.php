<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';


    protected $fillable = [
        'name'
    ];

    public function shops()
    {
        return $this->belongsToMany('App\Shop', 'book_shop');
    }

    public function publisher()
    {
        return $this->belongsTo('App\Publisher');
    }
}
