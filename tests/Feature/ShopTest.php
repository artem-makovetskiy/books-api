<?php

namespace Tests\Feature;

use App\Book;
use App\Publisher;
use App\Shop;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShopTest extends TestCase
{
    use WithoutMiddleware, DatabaseMigrations;

    /**
     * A basic feature test example.
     *
     * @return void
     */
//    public function testExample()
//    {
//        $response = $this->get('/');
//
//        $response->assertStatus(200);
//    }

    public function test_list_publisher_shops_with_atleast_one_book()
    {
//        $shops = factory(Shop::class, 5)->create()->all();
//        $publishers = factory(Publisher::class, 3)->create()->each(function ($publisher) use ($shops) {
//            $publisher->books()->saveMany(factory(Book::class, 5)->create()->each(function ($book) use ($shops) {
//                $book->shops()->attach($shops[array_rand($shops)]);
//            }));
//        });

        $publisher = factory(Publisher::class)->create();
        $shop = factory(Shop::class)->create();
        $books = factory(Book::class, 5)->create()->each(function ($book) use ($publisher, $shop) {
            $publisher->books()->save($book);
            $book->shops()->attach($shop);
        });

        $response = $this->json('GET', '/api/publishers/'.$publisher->id.'/shops');

//        fwrite(STDERR, print_r('/api/publishers/'.$publisher->id.'/shops', TRUE));

        $response
            ->assertStatus(200);
//            ->assertJsonFragment([
//                'shops' => [
//                    [
//                        'id' => $shop->id,
//                        'books_in_stock' => [
//                            'id' => $books[0]->id,
//                        ]
//                    ]
//                ]
//            ]);
//            ->assertJson([
//                'shops' => [
//                    [
//                        'id' => $shop->id,
//                        'books_in_stock' => [
//                            'id' => $books[0]->id,
//                        ]
//                    ]
//                ]
//            ]);

    }

    public function test_mark_books_as_sold_for_shop()
    {
        $shop = factory(Shop::class)->create();
        $book = factory(Book::class)->create();
        $book->shops()->attach($shop);

        $sold_count = 1;

//        fwrite(STDERR, print_r('/api/shops/'.$shop->id.'/books/'.$book->id, TRUE));

        $response = $this->json('PUT', '/api/shops/'.$shop->id.'/books/'.$book->id.'?sold='.$sold_count);

        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => $shop->id,
                'books_sold_count' => $shop->books_sold_count + $sold_count
            ]);
    }
}
