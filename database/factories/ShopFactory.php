<?php

use Faker\Generator as Faker;

$factory->define(App\Shop::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'books_sold_count' => $faker->numberBetween(0, 10)
    ];
});

//$factory->afterCreating(App\Shop::class, function ($shop, $faker) {
//    for ($i = 1; $i <= 10; $i++) {
//        $shop->books()->attach(factory(App\Book::class)->create());
//    }
//});
