<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'copies_in_stock' => $faker->numberBetween(0, 10),
        'publisher_id' => factory(App\Publisher::class)->create()->id
    ];
});

//$factory->afterCreating(App\Book::class, function ($book, $faker) {
//    $book->publisher()->associate(factory(App\Publisher::class)->create());
//});
